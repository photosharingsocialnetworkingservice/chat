using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Repositories
{
    public interface IConversationRepository
    {
        public Task AddNewConversation(Conversation conversation);
        public Task<bool> DoesConversationExist(int userId, int friendId);
        Task AddMessageToConversation(int userId, int friendId, Message message);
        Task<List<UnwindedConversation>> GetMessages(int userId, int friendId, DateTimeOffset? lastMessageDate);
    }
}