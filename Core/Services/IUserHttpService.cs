using System.Threading.Tasks;

namespace Core.Services
{
    public interface IUserHttpService
    {
        Task<bool> DoesFriendshipExist(string id1, string id2);
    }
}