using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Services
{
    public interface IConversationService
    {
        public Task AddNewConversation(int userId, int friendId);
        public Task<bool> DoesConversationExist(int userId, int friendId);
        public Task AddMessageToConversation(int senderId, int friendId, string message, DateTimeOffset date);
        public Task<List<Message>> GetMessages(int senderId, int friendId,
            DateTimeOffset? lastMessageDate);
    }
}