using System;
using System.Data;
using System.Net.Http;
using System.Threading.Tasks;

namespace Core.Services
{
    public class UserHttpService : IUserHttpService
    {
        private readonly HttpClient _client;

        public UserHttpService(HttpClient client)
        {
            var userServiceBaseUrl = Environment.GetEnvironmentVariable("USER_SERVICE_URL");
            
            if (userServiceBaseUrl == null)
                throw new NoNullAllowedException("User service url env variable does not exist");
            
            client.BaseAddress = new Uri(userServiceBaseUrl);
            _client = client;
        }

        public async Task<bool> DoesFriendshipExist(string id1, string id2)
        {
            var response = await _client.GetAsync($"friends/exist?id1={id1}&id2={id2}");
            return bool.Parse(await response.Content.ReadAsStringAsync());
        }
    }
}