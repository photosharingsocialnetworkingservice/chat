using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Repositories;

namespace Core.Services
{
    public class ConversationService : IConversationService
    {
        private readonly IConversationRepository _conversationRepository;

        public ConversationService(IConversationRepository conversationRepository)
        {
            _conversationRepository = conversationRepository;
        }

        public async Task AddNewConversation(int userId, int friendId)
        {
            var conversation = new Conversation
            {
                ParticipantsIds = new List<int>{ userId, friendId },
                Messages = new List<Message>()
            };

            await _conversationRepository.AddNewConversation(conversation);
        }

        public async Task<bool> DoesConversationExist(int userId, int friendId)
        {
            return await _conversationRepository.DoesConversationExist(userId, friendId);
        }

        public async Task AddMessageToConversation(int senderId, int friendId, string message, DateTimeOffset date)
        {
            var msg = new Message
            {
                SenderId = senderId,
                Content = message,
                Date = date
            };

            await _conversationRepository.AddMessageToConversation(senderId, friendId, msg);
        }

        public async Task<List<Message>> GetMessages(int senderId, int friendId,
            DateTimeOffset? lastMessageDate = null)
        {
            var unwindedConversations = await _conversationRepository.GetMessages(senderId, friendId, lastMessageDate);
            var messages = unwindedConversations
                .Select(s => s.Messages)
                .ToList();
            
            return messages;
        }
    }
}