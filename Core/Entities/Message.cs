using System;

namespace Core.Entities
{
    public class Message
    {
        public int SenderId { get; set; }
        public string Content { get; set; }
        public DateTimeOffset Date { get; set; }
    }
}