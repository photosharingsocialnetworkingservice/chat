using System.Security.Claims;
using Microsoft.AspNetCore.SignalR;

namespace Core
{
    public class UserIdProvider : IUserIdProvider
    {
        public virtual string GetUserId(HubConnectionContext connection)
        {
            return connection.User?.FindFirst("id")?.Value;
        }
    }
}