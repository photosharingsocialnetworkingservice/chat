using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Entities;
using Core.Repositories;
using Core.Services;
using Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConversationController : ControllerBase
    {
        private readonly IConversationService _conversationService;

        public ConversationController(IConversationService conversationService)
        {
            _conversationService = conversationService;
        }
        
        [Authorize]
        [HttpGet("messages")]
        public async Task<List<Message>> GetMessages(int friendId, DateTimeOffset? lastMessageDate)
        {
            var userId = int.Parse(User.FindFirstValue("id"));
            return await _conversationService.GetMessages(userId, friendId, lastMessageDate);
        }
    }
}