using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Api.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private readonly IUserHttpService _userHttpService;
        private readonly IConversationService _conversationService;

        public ChatHub(IUserHttpService userHttpService, IConversationService conversationService)
        {
            _userHttpService = userHttpService;
            _conversationService = conversationService;
        }
        
        [Authorize]
        public async Task SendMessage(string receiverId, string message)
        {
            var senderId = Context.UserIdentifier;
            var doesFriendshipExist = await _userHttpService.DoesFriendshipExist(senderId, receiverId);
            var date = DateTimeOffset.Now;

            var parsedSenderId = int.Parse(senderId ?? throw new Exception("User id is invalid"));
            var parsedReceiverId = int.Parse(receiverId ?? throw new Exception("Friend id is invalid"));
            
            if (doesFriendshipExist)
            {
                var doesConversationExist = await _conversationService.DoesConversationExist(parsedSenderId, parsedReceiverId);

                if (!doesConversationExist)
                {
                    await _conversationService.AddNewConversation(parsedSenderId, parsedReceiverId);
                }

                await _conversationService.AddMessageToConversation(parsedSenderId, parsedReceiverId, message, date);
                
                await Task.WhenAll(
                    new Task[]
                    {
                        Clients.User(senderId).SendAsync("message", senderId, message, date),
                        Clients.User(receiverId).SendAsync("message", senderId, message, date)
                    });
            }
        }
    }
}