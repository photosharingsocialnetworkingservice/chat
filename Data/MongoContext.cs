using System;
using Core.Entities;
using MongoDB.Driver;

namespace Data
{
    public class MongoContext
    {
        public IMongoCollection<Conversation> Conversations { get; set; }

        public MongoContext()
        {
            var connectionString = Environment.GetEnvironmentVariable("MONGO_CONNECTION_STRING");
            var databaseName = Environment.GetEnvironmentVariable("MONGO_DATABASE");
            var client = new MongoClient(connectionString);
            Conversations = client.GetDatabase(databaseName).GetCollection<Conversation>("conversation");
        }
    }
}