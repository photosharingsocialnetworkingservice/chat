using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Repositories;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Data.Repositories
{
    public class ConversationRepository : IConversationRepository
    {
        private readonly MongoContext _mongoContext;

        public ConversationRepository(MongoContext mongoContext)
        {
            _mongoContext = mongoContext;
        }

        public async Task AddNewConversation(Conversation conversation)
        {
            await _mongoContext.Conversations.InsertOneAsync(conversation);
        }

        public async Task<bool> DoesConversationExist(int userId, int friendId)
        {
            var foundDocument = await _mongoContext.Conversations
                .FindAsync(c =>
                    new List<int> {userId, friendId}.All(i => c.ParticipantsIds.Contains(i))
                )
                .Result
                .FirstOrDefaultAsync();

            return foundDocument != null;
        }

        public async Task AddMessageToConversation(int userId, int friendId, Message message)
        {
            var update = Builders<Conversation>.Update.Push(c => c.Messages, message);

            await _mongoContext.Conversations
                .UpdateOneAsync(
                    c =>
                        new List<int> {userId, friendId}.All(i => c.ParticipantsIds.Contains(i)),
                    update
                );
        }

        public async Task<List<UnwindedConversation>> GetMessages(int userId, int friendId, DateTimeOffset? lastMessageDate = null)
        {
            var result = await _mongoContext.Conversations
                .Aggregate()
                .Match(
                    new BsonDocument
                    {
                        {
                            "$expr",
                            new BsonDocument
                            {
                                {
                                    "$setIsSubset",
                                    new BsonArray(new List<BsonValue>
                                        {new BsonArray(new[] {userId, friendId}), "$ParticipantsIds"})
                                }
                            }
                        }
                    }
                )
                .Unwind<Conversation, UnwindedConversation>(u => u.Messages)
                .Match(u => lastMessageDate == null || u.Messages.Date < lastMessageDate)
                .Sort(new BsonDocument
                {
                    {
                        "Messages.Date",
                        -1
                    }
                })
                .Limit(25)
                .ToListAsync();

            return result;
        }
    }
}
